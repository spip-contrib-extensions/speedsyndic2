<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'speedsyndic_description' => 'Ce plugin permet d\'avoir une syndication automatique en temps réel de certains sites syndiqués choisis.',
	'speedsyndic_slogan' => 'Syndiquer vos sites plus rapidement',
);
