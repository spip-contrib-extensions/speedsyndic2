<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'erreur' => 'Error: ',
	'frequence' => 'Refresh interval: (in seconds)',
	'frequence_num' => 'Please enter a numberical value for the refresh interval.',
	'frequence_trente' =>  'The refresh interval cannot be inferior to 30.',
	'syndiclist' => 'Sites to speedsyndicate: ',

	// T
	'titre_page_configurer_speedsyndic' => 'Configure Speedsyndic',
);

