<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'erreur' => 'Erreur : ',
	'frequence' => 'Fréquence de rafraichissement (en secondes) : ',
	'frequence_num' => 'Veuillez entrer un nombre entier > 30 pour la fréquence.',
	'frequence_trente' =>  'La fréquence ne peut pas  &ecirc;tre infèrieure à 30.',
	'syndiclist' => 'Sites à speedsyndiquer : ',

	// T
	'titre_page_configurer_speedsyndic' => 'Configurer Speedsyndic',
);
